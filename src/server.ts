// import { Application } from "https://deno.land/x/oak/mod.ts";
import { Application, Router } from "https://deno.land/x/oak@6.0.1/mod.ts";
import router from "./routes/routes.ts";
import { oakCors } from "https://deno.land/x/cors/mod.ts";
const port = 3000;
const app = new Application();

app.use(oakCors());
app.use(router.routes());
app.use(router.allowedMethods());

router.get("/", handlePage);
app.use(router.routes());
app.use(router.allowedMethods());

console.log(`Server run on port ${port}`);
await app.listen({ port: port });



function handlePage(ctx: any) {
  try {
    ctx.response.body = `<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body >
    <div id="root"><h1>Hello SSR</h1></div>
  </body>
  </html>`;
  } catch (error) {
    console.error(error);
  }
}
