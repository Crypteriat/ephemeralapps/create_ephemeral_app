
/*
 * Terms for smart contract payment per open source licensing addendum
 * for smartcontractid "0xaldfal4kafll3dffcw",
 * see: https://eips.ethereum.org/erc
 * emits link on event in ephemeral_link_payments object
 * may use import_map.json to instrument for distribution of payments
 */
const terms = {
  oninstall: true,
  opensource_map: './import_map.json', // may be set to false
  ephemeral_link_payments: {
    onInstall: 'https://ephemeralapp.org/0xc7749yqw',
    onStart: 'https://ephemeralapp.org/0xc8349yqw',
    onLink: 'https://ephemeralapp.org/0xc7780yqw',
    /* more to be added to the smart contract
       onUpdate: 'https://ephemeralapp.org/0xd8379yqh',
       onUpgrade: 'https://ephemeralapp.org/0xc8349yqw',
     */
  beneficiaries: {
    BTC: {
      neovim: '1Evu6wPrzjsjrNPdCYbHy3HT6ry2EzXFyQ',
      share: .1 /* if no share specified, defaults to even split */
      }
    },
    ETH: {
      deno: '0x6520a28adcd29C8E52007957160E5d2aEbF32a12'
    }
  }
};
export default terms;
