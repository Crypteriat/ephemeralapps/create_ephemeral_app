# Ephemeral Web App Generator: A Web 3.0 tool to create decentralized & composable FreshJS Progressive Web Apps

## Quckstart

```curl -fsSL https://deno.land/x/install/install.sh | sh # install deno```

```deno run -A https://gitlab.com/Crypteriat/create_ephemeral_app/-/raw/master/src/cli.ts <project-name> # answer questionairre; FOSS or sponsor, payment routes, ephemeral link reward terms```

```cd <project-name>```

code your FreshJS website which is a regular website that can run as progressive web app with built-in identity and payment infrastructure

```<edit - VSCode, vim> .sponsorship.ts # if FOSS project```

```<edit> Terms.ts # if commercial or FOSS project```

```deno run -A src/pilot-app.js```

This will create a basic AlephJS website that can produce or consume ephemeral links.

### Approach

![Execution and Linkage Model](./docs/execution_instantiation.png)

This tool enables decentralized applications ("DApps") to produce and consume ephemeral links. In addition, it provides an abstraction layer leveraging FreshJS Progressive Web Apps (PWAs) allowing
similar behavior to React Native appications. These DApps run on the Onion Collective's fog infrastructure. It is modeled after the [`create-react-app`](https://github.com/facebook/create-react-app)
functionality and interface.

![Base Vault plus Links Make Composable Apps](./docs/Ephemeral_Model.png)

##### Creates Apps Produce and Consume Ephemeral Links

As an example, a "base" ephemeral web application (EWA) can share private data with another "super EWA" which only has access to what the base application wishes to share. The "super EWA" may then
create a curated network of users via their handles to build a social netowrk. This approach allows end-users to own their own data and identity. This example is one of many that this approach enables.
New sharing, voting, membership models, etc. can be created by linking and composing DApps.

### Pilot Application

![Pilot Application](./docs/Link_PWA.png)

##### edit `src/index.js` to link Progressive Web App abstraction layer:
* Camera
* GPS
* Geofencing
* Accelerometer
* Compass
* Native Payments (Apple & Google)
* Contact list
* Alarm
* Calendar
* SMS and push notifications
* Near-field communication and mobile payments

navigate to `http://localhost.com/:3000`

##### run smart contract on testnet with your gwei, start developing...

## Test App: Personal Vault

![Demonstration Application](./docs/Vault.png)

As part of the `create-ewa` package a basic ephemeral app is offered. This app provides a basic application which
provides a basic personal vault. You can think of the basic model as to record on the fog infrastrcture's immutable
blockchain ledger changes to the vault's `Persist.json` file.

## Testing

TBD
