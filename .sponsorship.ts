/*
 * Metadata for payment routing and link consumption to payment oracle (ERC-33XX standard)
 * see: https://eips.ethereum.org/erc
 */
const config = {
    base: {
        testnet: {
            smartcontractid: "0xaldfal4kafll3dffcw",
            version: "0.2b",
            ephemeral_link_regexs: [
              /https:\/\/ephemeralapp.org\/0xc8349yqw.*/,
              /https:\/\/ephemeralapp.org\/0xaldfal4kafll3dffcw.*/
            ]
        },
        production: {}
    }
};
export default config;
